<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Products;
use App\Categories;
use App\Cart;

use Session;

class ProductController extends Controller
{
   
    public function index()
    {
        $products = Products::all();
        $categories = Categories::all();
        //return view('category_level')->with('products', $products,'categories',$categories);

        return view('category_level', ['products' => $products, 'categories'=>$categories ]);
    }



    public function filter(Request $request,$id){
        $id = $request->id;
        $products = DB::table('products')->where('category_id', $id)->get();
        $category= DB::table('categories')->where('Id', $id)->get();
        return view('king_store', ['products' => $products, "category"=>$category]);
    }


    public function cart(Request $request,$id){
        $id = $request->id;
       $product = Products::find($id);
       $oldCart = Session::has('cart') ? Session::get('cart') : null;
       $cart = new Cart($oldCart);
       $cart->add($product, $id);
        
       $request->session()->put('cart',$cart);

       $cart=" ";
      return redirect()->route('cart');
    }

    public function showCart(){
        //return view('store_cart');

        if(!Session::has('cart')){
            return view('store_cart',['products'=> null]);
        }

        $oldCart = Session::get('cart');
        $cart = new cart($oldCart);
        return view('store_cart',['products'=> $cart->items, 'totalPrice' => $cart->totalPrice]);
    }


    public function empty_cart(Request $request){
       $request->session('cart')->flush();
       $cart = " ";

       return view('store_cart');
    }



}
