<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'ProductController@index')->name('index');


Route::get('/filter/{id}', 'ProductController@filter')->name('filter');

Route::get('/add_cart/{id}', 'ProductController@cart')->name('addCart');

Route::get('/cart', 'ProductController@showCart')->name('cart');

Route::get('emptyCart', 'ProductController@empty_cart');