    
<?php include('includes/header.php'); ?>


    <div class="container-fluid">
      <div class="categories">
        <div class="center-block">

          <?php
            if($categories){
          ?>
          <ul class="cat-items">
           <?php 
              $i=1;
              foreach($categories as $category) {

           ?>
            <li class="item"><a href="<?php echo url('filter')."/".$category->Id; ?>"><img class="img-cat" src="<?php echo 'img/categories/cat'.$i.'.png'; ?>" /><span><?php echo $category->title; ?></span></a></li>
            
            <?php ++$i;} //end foreach ?>
          </ul>

          <?php
            } //end if
          ?>
        </div>
      </div>



    </div>

    <div class="products-crousel">
        <div id="owl-demo" class="owl-carousel owl-theme">
            <?php
             if($products){
          ?>
           <?php

              foreach($products as $product) {
            ?>
            <div class="item">

              <div class="container">
                <div class="row">
                   <div class="description col-xs-4">
                     <h3><?php echo $product->title; ?></h3>
                     <p>
                       <?php echo $product->description; ?>
                     </p>
                   </div>

                   <div class="image-product col-xs-8">

                     <div class="circle">
                      <?php
                       if($product->img_url){
                       
                      ?>
                      <img  src="<?php echo url($product->img_url); ?>">
                      <?php
                       }
                       else{

                        ?>
                        <img  class="default" src="<?php echo asset('img/default.jpg'); ?>">
                        <?php
                       }
                       ?>
                       
                     </div>
                     <!--href=" //echo url('cart')."/".$product->Id; "-->
                     <div class="price">
                       <div class="icon">
                          <a href="<?php echo route('addCart',['id'=>$product->Id]); ?>">

                             <img src="<?php echo asset('img/cart_white.png'); ?>" >
                          </a>
                       </div>
                       <div class="price-word"><span class="pr">ფასი:</span><span class="number"><?php echo $product->price; ?></span></div>
                     </div>

                   </div>

                   <div class="description responsive">
                     <h3><?php echo $product->title; ?></h3>

                     <div class="price">
                       <div class="price-word"><span class="number"><?php echo $product->price; ?></span></div>
                       <div class="icon">
                          <a href="<?php echo route('addCart',['id'=>$product->Id]); ?>">

                             <img src="<?php echo asset('img/cart_white.png'); ?>" >
                          </a>
                       </div>
                       
                     </div>

                   </div>

                 </div>

               </div>

            </div>
       
            <?php
          } //end for
              }// end if
            ?>
           
       
        </div>
      </div>

      <?php include('includes/footer.php'); ?>