
<?php require_once('includes/header.php'); ?>

    <div class="container ">
      <h4>კალათა</h4>
      <div class="container_table">
        
        <table class="table">
          <tr>
            <th>პროდუქტის დეტალები</th>
            <th> </th>
            <th>ფასი</th>
            <th class="count_th">რაოდენობა</th>
            <th class="total_count">ღირებულება</th>
            <th> </th>
          </tr>

          <?php

            if(Session::has('cart')){

              foreach ($products as $product) {
          ?>
          <tr>
            <td class="img-product">
              <div class="img_contain">
                <?php
                    if($product['item']['img_url']){
                      ?>
                      <img class="img-responsive" src="<?php echo $product['item']['img_url'];  ?>" >
                      <?php
                       }
                       else{

                        ?>
                        <img  class="default img-responsive" src="<?php echo asset('img/default.jpg'); ?>">
                        <?php
                       }
                ?>
              </div>
            </td>
            <td class="title">
              <span class="first"><?php echo $product['item']['title']; ?></span>
            </td>
            <td class="price">
              <span class="price"><?php echo $product['price']; ?></span>
            </td>

            <td class="count">

              <div class="circle_div">
                <span class="first fa fa-plus" id="plus" aria-hidden="true"></span>
                <span class="second" id="value_count"><?php echo $product['qty']; ?></span>
                <span class="third" id="minus"><i class="fa fa-minus" aria-hidden="true"></i></span>
              </div>
            </td>

            <td class="total">
              <span class="total-price"><?php  ?></span>
            </td>
            <td class="remove">
              
            </td>
          </tr>
          <?php 
            }//end of for
         

           ?>
          
        </table>

        <div class="total-calculate">
          <div class="pull-right">
            <span class="first">სულ ჯამში:</span>
            <span class="second"><?php $totalPrice; ?></span>
            <span class="third"><a href="<?php echo url('emptyCart'); ?>"><span class="glyphicon glyphicon-trash"></span></a></span>
          </div>
        </div>
       <?php
         } //end of if 
         else{
            ?>
          
          <div class="container">
            <h3>No Items In The Cart.</h3>
          </div>

         <?php   
         }
       ?>
       

      </div>
      
       <div class="button-checkout">
          <a class="pull-right" href="#">შეკვეთა</a>
       </div>
      

    </div>


    <?php require_once('includes/footer.php'); ?>