<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>King Store</title>

    <!-- Bootstrap -->
    <link href="<?php echo asset('/css/bootstrap.min.css'); ?>" rel="stylesheet">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Owl Carousel Assets -->
    <link href="<?php echo asset('/css/owl.carousel.css'); ?>" rel="stylesheet">
    <link href="<?php echo asset('/css/owl.theme.css'); ?>" rel="stylesheet">
   <link rel="stylesheet" href="https://cdn.web-fonts.ge/fonts/bpg-web-001-caps/css/bpg-web-001-caps.min.css">
    <!-- mystyle -->
    <link href="<?php echo asset('/css/mystyle.css'); ?>" rel="stylesheet">
    <link href="<?php echo asset('/css/responsive.css'); ?>" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <?php
  $class_name=" ";
   $route_name=Route::currentRouteName();
   if($route_name=='cart'){$class_name="store_cart";}
   elseif($route_name=='filter'){$class_name="king_store";}
  ?>
  <body class="<?php echo $class_name; ?>">

    <nav class="navbar navbar-default my-menu">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo route('index'); ?>"><img src="<?php echo asset('/img/logo.png'); ?>" > <span class="logo-word">KINGS STORE</span></a>

          <ul class="items-responsive" >
            <li><a href="#" ><img src="<?php echo asset('/img/user_white.png'); ?>"></a></li>
            <li><a href="#"><img src="<?php echo asset('/img/cart_responsive.png'); ?>"><span class="number-circle"><i class="number">2</i></span></a></li>
          </ul>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li ><a href="#">ჩვენ შესახებ</a></li>
            <li><a href="#">წესები და პირობები</a></li>
            <li><a href="#">კონტაქტი</a></li>
          </ul>

          <ul class="nav navbar-nav navbar-right the-last-one">
            <li class="special-close"><a href="#"><img src="<?php echo asset('/img/close.png'); ?>"></a></li>
            <li><a href="#"><img src="<?php echo asset('/img/user_icon.png'); ?>" class="img-responsive"></a></li>
            <li><a href="<?php echo route('cart'); ?>"><img src="<?php echo asset('/img/cart.png'); ?>" class="img-responsive"><span class="number-circle"><i class="number">
              <?php 
                 //Session::has('cart') ? Session::get('cart')->totalQty : ' ';
              if(Session::has('cart')){

               $qty=Session::get('cart')->totalQty;
               echo $qty;
              }
              else{
                echo '0';
              }
              
              ?>
            </i></span></a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>