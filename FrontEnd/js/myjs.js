$(document).ready(function() {
 
  $("#owl-demo").owlCarousel({
 
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true,
      navigationText:false
 
      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
 
  });

   

   $('#plus').click(function(){
    var value_span=parseInt($('#value_count').text()); 
      new_value =value_span+1;
      $('#value_count').text(new_value);
   });

   $('#minus').click(function(){
    var value_span=parseInt($('#value_count').text()); 
     if(value_span>1){
      
      new_value =value_span-1;
      $('#value_count').text(new_value);
     }
      
   });
 
});