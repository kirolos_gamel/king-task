-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 29, 2017 at 05:59 PM
-- Server version: 5.7.17-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `king_task`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `Id` int(6) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(6) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`Id`, `title`, `parent_id`, `timestamp`) VALUES
(1, 'clothes', 0, '2017-07-29 09:23:40'),
(2, 'Paper Goods', 0, '2017-07-29 09:23:48'),
(3, 'Cleaners ', 0, '2017-07-29 09:26:21'),
(4, 'Personal Care', 0, '2017-07-29 09:26:21');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `Id` int(6) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `category_id` int(6) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `img_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`Id`, `title`, `description`, `price`, `category_id`, `timestamp`, `img_url`) VALUES
(1, 'მაისური კინგსის სიმბოლოკით', 'პოლოს ტიპის კლასიკური მაისური. კომფორტული, თანამედროვე მოდელი. დამზადებულია გაუმჯობესებული ხარისხის ბამბის ნართისგან. ', 25, 1, '2017-07-29 12:16:38', 'T-shirt.png'),
(2, 'მაისური კინგსის სიმბოლოკით', 'პოლოს ტიპის კლასიკური მაისური. კომფორტული, თანამედროვე მოდელი. დამზადებულია გაუმჯობესებული ხარისხის ბამბის ნართისგან. ', 15, 1, '2017-07-29 09:29:24', ''),
(3, 'მაისური კინგსის სიმბოლოკით', 'პოლოს ტიპის კლასიკური მაისური. კომფორტული, თანამედროვე მოდელი. დამზადებულია გაუმჯობესებული ხარისხის ბამბის ნართისგან. ', 8, 2, '2017-07-29 09:32:03', ''),
(4, 'მაისური კინგსის სიმბოლოკით', 'პოლოს ტიპის კლასიკური მაისური. კომფორტული, თანამედროვე მოდელი. დამზადებულია გაუმჯობესებული ხარისხის ბამბის ნართისგან. ', 5, 3, '2017-07-29 09:32:03', ''),
(5, 'მაისური კინგსის სიმბოლოკით', 'პოლოს ტიპის კლასიკური მაისური. კომფორტული, თანამედროვე მოდელი. დამზადებულია გაუმჯობესებული ხარისხის ბამბის ნართისგან. ', 7, 4, '2017-07-29 09:35:05', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `Id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `Id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
